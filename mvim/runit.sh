set -x
workDir="/root${PWD#$HOME}"
docker run -it -v $PWD:$workDir -v ~/.vim/tmp:/root/.vim/tmp -v ~/.vim/data:/root/.vim/data -w $workDir --entrypoint /bin/bash kamcpp/vim:latest
