# User specific aliases and functions
export TERM=screen-256color
export PATH=$PATH:$HOME/.local/bin:/usr/local/bin:$HOME/bin:$HOME/.mybin

export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_COLLATE=C
export LC_CTYPE=en_US.UTF-8

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# some more ls aliases
alias ll='ls -alGF'
alias la='ls -AG'
alias l='ls -CGF'
alias ls='ls -G'
alias gitcanepf='git aa & git cane && git pof'

alias tmux="TERM=screen-256color tmux"
export PATH="/usr/local/opt/openjdk/bin:$PATH"

export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=1000000                  # big big history
export HISTFILESIZE=1000000              # big big history
# important: use shopt -s for bash. setopt works with zsh.
setopt histappend                        # append to history, don't overwrite it

# Save and reload the history after each command finishes
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

export PATH="$HOME/.composer/vendor/bin:$PATH"
. "$HOME/.cargo/env"
