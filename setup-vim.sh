#!/bin/bash

DESIRED_CMAKE_VERSION=3.22.5
DESIRED_VIM_VERSION=8.2.3227
DESIRED_GOLANG_VERSION=go1.18.3

if [[ $EUID -eq 0 ]]; then
    echo "ERROR: Don't run this script in sudo mode"
    exit 1
fi

thisDir=$PWD

unameOut="$(uname -s)"
case "${unameOut}" in
  Linux*)     machine=Linux;;
  Darwin*)    machine=Mac;;
  CYGWIN*)    machine=Cygwin;;
  MINGW*)     machine=MinGw;;
  *)          machine="UNKNOWN:${unameOut}"
esac

archOut="$(arch)"
case "${archOut}" in
  aarch64*)   archi=arm64;;
  arm64*)     archi=arm64;;
  *)          archi=amd64
esac

echo "Detected OS: '${machine}'"
echo "Detected Arch: '${archi}'"

sudo rm -rf $thisDir/.tmp
mkdir -p $thisDir/.tmp

do_apt() {
    echo "Updating apt-get ..."
    sudo apt-get update
    echo "Installing apt-get packages ..."
    sudo apt-get -y install build-essential git tmux ncurses-dev libpython3-dev cscope python3 g++ default-jdk libssl-dev silversearcher-ag
}

fix_compiler() {
    sudo apt-get -y install g++-8
    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 700 --slave /usr/bin/g++ g++ /usr/bin/g++-7
    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 800 --slave /usr/bin/g++ g++ /usr/bin/g++-8
}

install_cmake() {
    echo "Checking cmake version ..."
    echo "Desired cmake version: ${DESIRED_CMAKE_VERSION}"
    CMAKE_VERSION=`cmake --version | head -n 1 | grep -oP '[0-9|.]+'`
    echo "Found cmake version: $CMAKE_VERSION ..."
    if [[ "$CMAKE_VERSION" != "${DESIRED_CMAKE_VERSION}"  ]]; then
        echo "Removing currently installed cmake ..."
        sudo apt-get -y purge cmake
        sudo rm -rf /usr/bin/cmake
        echo "Building cmake ..."
        cd $thisDir/.tmp
        git clone https://github.com/Kitware/CMake.git cmake
        cd cmake
        git checkout "v${DESIRED_CMAKE_VERSION}"
        ./bootstrap && make && sudo make install
        echo "cmake installed"
    else
        echo "Correct cmake is found"
    fi
}

install_golang() {
    echo "Checking golang version ..."
    echo "Desired golang version: ${DESIRED_GOLANG_VERSION}"
    GOLANG_VERSION=`go version | grep -oP 'go[0-9|.]+'`
    echo "Found golang version: $GOLANG_VERSION ..."
    if [[ "$GOLANG_VERSION" != "${DESIRED_GOLANG_VERSION}"  ]]; then
        echo "Installing golang ..."
        goTarball=${DESIRED_GOLANG_VERSION}.linux-$archi.tar.gz
        sudo rm -rf /usr/local/go
        wget https://go.dev/dl/$goTarball
        sudo tar -C /usr/local -xzf $goTarball
        rm -rf $goTarball
        # echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.bashrc
        source ~/.bashrc
        go version
        echo "golang installed"
    else
        echo "Correct golang is found"
    fi
}

install_vim() {
    echo "Checking vim version ..."
    echo "Desired vim version: ${DESIRED_VIM_VERSION}"
    VIM_VERSION=`vim --version | head -n 1 | grep -oP '8.[0-9|.]+'`
    VIM_PATCH_NR=`vim --version | head -n 2 | grep -oP '1-[0-9|.]+' | cut -d'-' -f2`
    VIM_GIT_VERSION="${VIM_VERSION}.${VIM_PATCH_NR}"
    echo "Found vim version: $VIM_GIT_VERSION ..."
    if [[ "$VIM_GIT_VERSION" != "${DESIRED_VIM_VERSION}"  ]]; then
        echo "Removing currently installed vim ..."
        sudo apt-get -y purge vim
        echo "Building vim ..."
        cd $thisDir/.tmp
        git clone https://github.com/vim/vim.git vim
        cd vim
        git checkout v8.2.3227
        ./configure --enable-cscope \
                    --enable-mu ltibyte \
                    --with-features=huge \
                    --enable-python3interp=yes \
                    --enable-rubyinterp=yes \
                    --enable-luainterp=yes \
                    --prefix=/usr/local
        make
        sudo make install
        echo "vim installed"
    else
        echo "Correct vim is found"
    fi
}

setup_dot_vim_dir() {
    cd $thisDir
    echo "Removing .vim ..."
    sudo rm -rf ~/.vim

    echo "Creating data and tmp dirs ..."
    mkdir -p ~/.vim/data
    mkdir -p ~/.vim/tmp

    echo "Creating vim symlinks ..."
    ln -sfv $PWD/mvim/vimrc ~/.vimrc
    mkdir -p ~/.vim

    mkdir -p ~/.vim/colors
    cp $PWD//mvim/colors/* ~/.vim/colors/
}

setup_vundle() {
    mkdir -p ~/.vim/bundle
    cd ~/.vim/bundle
    git clone https://github.com/VundleVim/Vundle.vim
}

vundle_update() {
    vim +'VundleUpdate' +'qall'
}

setup_ycm() {
    echo "Installing YouCompleteMe ..."
    cd ~/.vim/bundle/YouCompleteMe
    git checkout 4237c4647ec30215223d597f6172c8c46b8b239e
    python3 install.py --clang-completer --java-completer --go-completer
}

do_apt
fix_compiler
install_cmake
install_golang
install_vim
setup_dot_vim_dir
setup_vundle
vundle_update
setup_ycm

rm -rf $thisDir/.tmp
echo "Done"
