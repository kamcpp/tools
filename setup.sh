#!/bin/bash

unameOut="$(uname -s)"
case "${unameOut}" in
  Linux*)     machine=Linux;;
  Darwin*)    machine=Mac;;
  CYGWIN*)    machine=Cygwin;;
  MINGW*)     machine=MinGw;;
  *)          machine="UNKNOWN:${unameOut}"
esac
echo "Detected OS: '${machine}'"

echo "Installing packages ..."
sudo apt-get -y install git tmux

echo "Installing tmux plugins ..."
mkdir -p ~/.tmux/plugins
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

echo "Creating symlinks ..."

if [[ "$machine" = "Linux" ]]; then
  ln -sfv $PWD/bashrc ~/.bashrc
else
  ln -sfv $PWD/profile ~/.profile
fi

ln -sfv $PWD/bin ~/bin
ln -sfv $PWD/tmux.conf ~/.tmux.conf
ln -sfv $PWD/gitconfig ~/.gitconfig

echo "Done"
